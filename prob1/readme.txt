PROB1A
--------------------------------------------------------------------------------

* The mesh is `mesh1.gen`. Needs to be scaled by 0.001 (mm -> m).

* Simple heat conduction. No induction heating or enclosure radiation.

* There is a single (1-phase) graphite material assigned to all element
  blocks (2, 3, 4, 5). Density 1750; specific heat 1500; conductivity 100.

* Initial temperature 1500 all bodies.

* Boundary conditions

  * No heat flux on sides set 1 (symmetry planes)
  * HTC on side set 2: coef 50; ref temp 300
  * Simple radiation on side sets 20, 21, 22: emissivity 0.75, amb temp 300
  * No heat flux on side sets 10, 11, 23

* Interface conditions

  - HTC on side set 30: coef 500
  - Gap radiation on sideset 40: emissivity 0.75
  - HTC on side sets 41, 42: coef 500
  - HTC on side sets 50, 51: coef 200

* Numerical parameters

  - relative temperature and enthalpy tolerance: 1e-3
  - absolute temperature and enthalpy tolerance: 0
  - initial time step: 1e-2
  - dt_grow = 5.0

* Start at t=0, output every 60 sec to t = 600, then every 300 sec to
  final time 3600.


PROB1B
--------------------------------------------------------------------------------

Same as Prob1a except use temperature-dependent material properties.

* Cp(T) = 2.406e2 + 2.50167 T - 1.1231e-3 T^2 + 1.7e-7 T^3

* k(T) = 112.3504 - 0.13876097 T + 9.042e-5 T^2 - 2.0e-8 T^3

Also uses a position-dependent initial temperature profile:

* T(x,y,z) = 1500 - 1000 (x^2 + y^2) + 100 z


PROB1C
--------------------------------------------------------------------------------

Same as Prob1a except that we add a void-filled volume to the mesh.

* The mesh is `mesh2.gen`. Needs to be scaled by 0.001 (mm -> m).

* Element block 1 (the casting volume) is the new block, and is assigned the
  void material. Initial temperature is arbitrary (and insignificant), say 0.

* Additional boundary condition to prob1a:

  - No heat flux on side set 6

* Additional interface condition to probla:

  - HTC on side set 24; coef 0
